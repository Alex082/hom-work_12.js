/*
Чому для роботи з input не рекомендується використовувати клавіатуру?

есть не сколько причин: но я думаю что основная причина это
 из-за того, что функционал перемешивается с разметкой, что затрудняет работу и отладку кода
*/

// получаем кнопки
const buttons = document.querySelectorAll(".btn");

// сохраняем виделенную кнопку
let currentButton = null;

// функцыя для смены цвета
function changeButtonColor(button, color) {
  button.style.backgroundColor = color;
}

// keyup для выявления какая кнопка нажата
document.addEventListener("keydown", function (event) {
  // кнопка на которой написана, нажата кнопка
  const button = Array.from(buttons).find(
    (button) =>
      button.textContent === event.key.toUpperCase() ||
      (event.code === "Enter" && button.textContent === "Enter"));

  // делаем проверку есть ли такая кнопка и меняем её цвет
  if (button) {
    if (currentButton !== null) {
      changeButtonColor(currentButton, "#000000");
    }

    // сохраняем новуюю выделенную кнопку
    currentButton = button;

    // меняем цвет новой выделенной кнопку
    changeButtonColor(currentButton, "#0000FF");
  }
});
